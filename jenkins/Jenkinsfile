pipeline {
    agent {
        docker { image 'python:3.8.10' }
    }
    environment {
        QUALITY_DOCKER="0"
        BACKEND_IMAGE_TAG="latest"
        REGISTRY_IMAGE="registry.gitlab.com/edinio/weathersoundbackend"
        REGISTRY_USER="gbazack"
        REGISTRY_TOKEN="glpat-YzDy1UNZDsrZtizWJAyx"
        REGISTRY_URL="https://registry.gitlab.com"
    }

    stages {
        // Gitlab repository checkout
        stage('gilab-reporsitory-checkout') {
            steps {
                echo 'Checkout'
                checkout([$class: 'GitSCM', branches: [[name: '*/main']], browser: [$class: 'GitLabBrowser', projectUrl: 'https://gitlab.com/edinio/weathersoundbackend'], extensions: [], userRemoteConfigs: [[url: 'https://gitlab.com/edinio/weathersoundbackend']]])
            }
        }
        stage('Init') {
            steps {
                echo 'Preparing the environment'
                sh 'make set-test-env'
            }
        }
        // Stage: Security
        stage('Security') {
            parallel {
                // Bandit test
                stage('Bandit-test') {
                    steps {
                        echo "Scanning common security issues in Python code"
                        sh '. bandit-env/bin/activate'
                        sh 'make bandit-test'
                    }
                    options { warnError('JOB FAILED') }
                    post{
                        failure{
                            catchError(message: 'TEST FAILED', stageResult: 'FAILURE') {
                                sh 'exit 0'
                            }
                        }
                        unsuccessful{ echo "Test failed" }
                    }
                }
                // Safety-check
                stage('Safety-check') {
                    steps {
                        echo "Check installed dependencies for known vulnerabilities"
                        sh 'make safety-check'
                    }
                }
            }
        }
        // Stage: Code quality
        stage('Codequality') {
            parallel{
                // Docker-linter
                stage('Hadolint'){
                    agent {
                        docker { image 'hadolint/hadolint:v2.0.0-alpine' }
                    }
                    steps{
                        echo "Lint the dockerfile and docker-compose files"
                        writeFile file: 'reports/hadolint.json', text: ''
                        sh 'hadolint -f json Dockerfile | tee -a reports/hadolint.json'
                    }
                    post{
                        failure{
                            catchError(message: 'HADOLINT - LINTING FAILED', stageResult: 'FAILURE'){
                                sh 'exit 0'
                            }
                        }
                        always{
                            recordIssues aggregatingResults: true, enabledForFailure: true, healthy: 1, minimumSeverity: 'HIGH', tools: [dockerLint(pattern: 'reports/hadolint.json'), hadoLint(pattern: 'reports/hadolint.json')], unhealthy: 9
                        }
                    }
                }
                // Python-flake8
                stage('Python-Flake8'){
                    steps{
                        echo "Static testing with Python Flake8 package"
                        sh '. bandit-env/bin/activate'
                        writeFile file: 'reports/flake8.json', text: ''
                        sh 'python -m pip install flake8 coverage'
                        sh 'flake8 --exit-zero --statistics WeatherSound/ | tee -a reports/flake8.json'
                        sh 'coverage run --source=app -m flake8 --exit-zero --ignore=E121,E122,E126,E127,E128,E241,E251,E265,F401,F403,F405,W391 --max-line-length 150 WeatherSound'
                    }
                    post{
                        failure{
                            catchError(message: 'FLAKE8 - CODE QUALITY TEST FAILED', stageResult: 'FAILURE'){
                                sh 'exit 0'
                            }
                        }
                        always {
                            recordIssues aggregatingResults: true, enabledForFailure: true, healthy: 1, minimumSeverity: 'HIGH', tools: [flake8(pattern: 'reports/flake8.json')], unhealthy: 9
                        }
                    }
                }
                // Pylama
                stage('Pylama'){
                    steps{
                        echo "Python code audit with pylama"
                        sh '. bandit-env/bin/activate'
                        //touch file: 'reports/pylama.json', timestamp: 0
                        writeFile file: 'reports/pylama.json', text: ''
                        sh 'python -m pip install pylama[all] coverage'
                        //sh 'coverage run -m pylama --ignore=E121,E122,E126,E127,E128,E241,E251,E265,F401,F403,F405,W391 --max-line-length 150 --max-complexity 9 WeatherSound'
                        sh 'pylama WeatherSound --format json | tee -a reports/pylama.json'
                    }
                    post{
                        failure{
                            catchError(message: 'CODE AUDIT FAILED', stageResult: 'FAILURE'){
                                sh 'exit 0'
                            }
                        }
                        //always{
                        //    recordIssues aggregatingResults: true, enabledForFailure: true, healthy: 1, minimumSeverity: 'HIGH', tools: [pep8(pattern: 'reports/pylama.json'), pyDocStyle(pattern: 'reports/pylama.json')], unhealthy: 9
                        //}
                    }
                }
            }
        }
        // Code coverage
        stage('Coverage'){
            steps{
                echo "Code coverage with Coverage.py"
                sh '. bandit-env/bin/activate'
                writeFile file: 'reports/coverage.xml', text: ''
                //touch file: 'reports/coverage.xml', timestamp: 0
                sh 'python -m pip install coverage'
                sh 'coverage report --no-skip-covered -m WeatherSound/*.py WeatherSound/WeatherSound/*.py WeatherSound/api/*.py WeatherSound/api/migrations/*.py WeatherSound/api/management/commands/*.py' //--format json --output-file reports/flake8.json'
                sh 'coverage html --no-skip-covered --show-contexts --ignore-errors WeatherSound/*.py WeatherSound/WeatherSound/*.py WeatherSound/api/management/commands/*.py WeatherSound/api/*.py WeatherSound/api/migrations/*.py'
                sh 'coverage xml --ignore-errors WeatherSound/*.py WeatherSound/WeatherSound/*.py WeatherSound/api/*.py WeatherSound/api/migrations/*.py WeatherSound/api/management/commands/*.py'
            }
            post{
                failure{
                    catchError(message: 'COVERAGE - FAILED', stageResult: 'FAILURE'){
                        sh 'exit 0'
                    }
                }
                always {
                    publishHTML([allowMissing: false, alwaysLinkToLastBuild: true, includes: '**/*.html,**/*.css,**/*.js,**/*.png,**/*.jpg', keepAll: false, reportDir: 'htmlcov', reportFiles: 'index.html', reportName: 'Code Coverage Report', reportTitles: 'Python Code Coverage Report'])
                }
            }
        }
        // Stage: Building docker image
        stage('Build-Docker-image') {
            //agent {
            //    docker { image 'docker:20.10' }
            //}
            steps {
                echo "Building the docker image"
                script {
                    withDockerRegistry(url: '${REGISTRY_URL}') {
                        sh 'docker login -u "${REGISTRY_USER}" -p "${REGISTRY_TOKEN}" "${REGISTRY_URL}"'
                        def Image = docker.build("${REGISTRY_IMAGE}:${BACKEND_IMAGE_TAG}")
                        Image.push()
                    }
                }
            }
            options { warnError('BUILD JOB FAILED') }
            post{
                failure{
                    catchError(message: 'DOCKER IMAGE - BUILD  FAILED', stageResult: 'FAILURE') {
                        sh 'exit 0'
                    }
                }
            }
        }
    }
    
}

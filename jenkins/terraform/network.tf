# Module for deploying network instances

data "openstack_networking_network_v2" "external-network" {
    name = "external"
}

resource "openstack_networking_network_v2" "private-network" {
    name = "private-network"
}

resource "openstack_networking_subnet_v2" "private-subnet" {
    name            = "private-subnet"
    network_id      = openstack_networking_network_v2.private-network.id
    cidr            = "192.168.1.0/24"
    ip_version      = 4
    dns_nameservers = ["192.44.75.10", "192.108.115.2"]
}

resource "openstack_networking_router_v2" "private-router" {
    name                = "private-router"
    external_network_id = data.openstack_networking_network_v2.external-network.id
}

resource "openstack_networking_router_interface_v2" "private-router-interface" {
    router_id = openstack_networking_router_v2.private-router.id
    subnet_id = openstack_networking_subnet_v2.private-subnet.id
}

resource "openstack_networking_floatingip_v2" "floating-ip" {
    pool = data.openstack_networking_network_v2.external-network.name
}

resource "openstack_networking_floatingip_v2" "floating-ip-compute" {
    pool = data.openstack_networking_network_v2.external-network.name
}
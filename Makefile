all: auth pull

auth:
	@docker login -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" "${CI_REGISTRY}"

pull:
	@docker pull "${BACKEND_IMAGE_TAG}"
	@docker image tag "${BACKEND_IMAGE_TAG}" backend:latest
	@docker rmi "${BACKEND_IMAGE_TAG}"
	@docker pull "${FRONTEND_IMAGE_TAG}"
	@docker image tag "${FRONTEND_IMAGE_TAG}" frontend:latest
	@docker rmi "${FRONTEND_IMAGE_TAG}"

compose:
	@docker-compose up --detach

clean:
	@docker rmi backend:latest frontend:latest

ansible-help:
	@ansible-playbook -h

deploy:
	@ansible-playbook -i "${ANSIBLE_HOSTS}" --private-key "${ANSIBLE_SSH_KEY}" "${ANSIBLE_PLAYBOOKS}"/deploy.yaml

test:
	@ansible-playbook -i "${ANSIBLE_HOSTS}" --private-key "${ANSIBLE_SSH_KEY}" "${ANSIBLE_PLAYBOOKS}"/test.yaml
